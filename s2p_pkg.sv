package s2p_pkg;
  import uvm_pkg::*;
  `include "uvm_macros.svh"
  
  class s2p_trans extends uvm_sequence_item;
    logic [15:0] data_16b;
	bit [15:0] first_data;
	bit [15:0] last_data;
    bit rsp;
	realtime start_time;

    `uvm_object_utils_begin(s2p_trans)
      `uvm_field_int(data_16b, UVM_ALL_ON)
      `uvm_field_int(rsp, UVM_ALL_ON)
	  `uvm_field_int(first_data, UVM_ALL_ON)
	  `uvm_field_int(last_data, UVM_ALL_ON)
    `uvm_object_utils_end

    function new (string name = "s2p_trans");
      super.new(name);
    endfunction  
  endclass:s2p_trans

  class s2p_driver extends uvm_driver #(s2p_trans);
	`uvm_component_utils(s2p_driver)
	
	function new(string name = "s2p_driver", uvm_component parent);
	  super.new(name,parent);
	endfunction    
  endclass:s2p_driver

  class s2p_monitor extends uvm_monitor;
    local virtual s2p_intf intf;
	local virtual d2s_intf d2s_vif;
	uvm_analysis_port #(s2p_trans) mon_ana_port;
	
    `uvm_component_utils(s2p_monitor)
    function new(string name="s2p_monitor", uvm_component parent);
      super.new(name, parent);
      mon_ana_port = new("mon_ana_port", this);
    endfunction	
	
    function void set_interface(virtual s2p_intf intf,virtual d2s_intf d2s_vif);
      if(intf == null)
        $error("s2p interface handle is NULL, please check if target interface has been intantiated");
      else
        this.intf = intf;
	  if(d2s_vif == null)
        $error("d2s interface handle is NULL, please check if target interface has been intantiated");
      else
        this.d2s_vif = d2s_vif;
    endfunction	
  
    task run_phase(uvm_phase phase);
      this.mon_trans();
    endtask
	
	task mon_trans();
	  s2p_trans m;
	  forever begin
	    @(posedge d2s_vif.ack);
		m = s2p_trans::type_id::create("m");
		m.data_16b = intf.mon_ck.data_16b;
		if($isunknown(m.data_16b)) continue;
	    m.start_time = $realtime();
		mon_ana_port.write(m);
		`uvm_info(get_type_name(),$sformatf("monitored s2p data 4'b%x",m.data_16b),UVM_HIGH);
	  end
	endtask
  endclass : s2p_monitor



  class s2p_agent extends uvm_agent;
    s2p_driver driver;
	s2p_monitor monitor;
	local virtual s2p_intf s2p_vif;
	local virtual d2s_intf d2s_vif;
	
    `uvm_component_utils(s2p_agent)

    function new(string name = "s2p_agent", uvm_component parent);
      super.new(name, parent);
    endfunction	
  
    function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      // get virtual interface
      if(!uvm_config_db#(virtual s2p_intf)::get(this,"","s2p_vif", s2p_vif)) begin
        `uvm_fatal("GETVIF","cannot get s2p_vif handle from config DB")
      end
      if(!uvm_config_db#(virtual d2s_intf)::get(this,"","d2s_vif", d2s_vif)) begin
        `uvm_fatal("GETVIF","cannot get d2s_vif handle from config DB")
      end	  
      monitor = s2p_monitor::type_id::create("monitor", this);
    endfunction
	
	function void connect_phase(uvm_phase phase);
      super.connect_phase(phase);
      this.set_interface(d2s_vif,s2p_vif);	  
    endfunction

    function void set_interface(virtual d2s_intf vif,virtual s2p_intf s2p_vif);
      monitor.set_interface(s2p_vif,vif);
    endfunction  
  
  endclass:s2p_agent


endpackage