`timescale 1ns/1ps
`define halfperiod 50

import uvm_pkg::*;
 `include "uvm_macros.svh"

interface d2s_intf(input sclk, input rstn);
  logic [3:0] data;
  logic ack;
  clocking drv_ck @(posedge sclk);
    default input #1ps output #1ps;
	input ack;
	output data;
  endclocking
  clocking mon_ck @(posedge sclk);
    default input #1ps output #1ps;
	input ack,data;
  endclocking
endinterface

interface s2p_intf(input sclk, input rstn);
  logic [15:0] data_16b;
  clocking mon_ck @(posedge sclk);
    default input #1ps output #1ps;
	input data_16b;
  endclocking
endinterface

//create this interface for d2s timing check
interface dt_intf(input sclk,input rstn);
  logic ack,scl,sda;
  clocking mon_ck @(posedge sclk);
    default input #1ps output #1ps;
    input ack,scl,sda;
  endclocking  

  property sda_after_ack;
    @(negedge sclk) $rose(ack)|=>$rose(sda);
  endproperty:sda_after_ack
  assert property(sda_after_ack) else `uvm_error("ASSERT","ack is not rise after ack")
  
  property sda_down_while_scl_high;
    @(negedge sclk) ($rose(sda) ##[1:2] $fell(sda))and(ack)  |-> $fell(scl);//注意$rose和$fell的判定机制
  endproperty:sda_down_while_scl_high
  assert property(sda_down_while_scl_high) else `uvm_error("ASSERT","sda fell assertion 1 error")
  
  property sda_down_while_scl_high2;
    @(negedge sclk) ($rose(sda) ##2 $fell(sda))and(!ack) |-> $rose(scl);
  endproperty:sda_down_while_scl_high2
  assert property(sda_down_while_scl_high2) else `uvm_error("ASSERT","sda fell assertion 2 error")  
  
  initial begin: assertion_control
    fork
      forever begin
        wait(rstn == 0);
        $assertoff();
        wait(rstn == 1);
        $asserton();
      end
    join_none
  end
endinterface




module tb;
logic sclk;
logic rstn;

initial begin
  rstn = 1;
  sclk = 0;
  #10 rstn = 0;
  #(`halfperiod*2+3) rstn = 1;
  forever begin 
		#(`halfperiod) sclk = ~sclk;
  end
end

top top_inst(
  	.rstn(rstn),
	.sclk(sclk),
	.data(d2s_if.data),
	.ack(d2s_if.ack),
	.parallel_out(s2p_if.data_16b)
	);

import uvm_pkg::*;
 `include "uvm_macros.svh"
import dut_pkg::*;	
	
d2s_intf d2s_if(.*);
s2p_intf s2p_if(.*);
dt_intf dt_if(.*);

assign dt_if.ack = tb.top_inst.ack_d2s_top;
assign dt_if.scl = tb.top_inst.scl_d2s_s2p;
assign dt_if.sda = tb.top_inst.sda_d2s_s2p;

initial begin
  uvm_config_db#(virtual d2s_intf)::set(uvm_root::get(), "uvm_test_top.env.d2s_agt", "d2s_vif", d2s_if);
  uvm_config_db#(virtual d2s_intf)::set(uvm_root::get(), "uvm_test_top.env.s2p_agt", "d2s_vif", d2s_if);
  uvm_config_db#(virtual s2p_intf)::set(uvm_root::get(), "uvm_test_top.env.s2p_agt", "s2p_vif", s2p_if);
  uvm_config_db#(virtual s2p_intf)::set(uvm_root::get(), "uvm_test_top.env.chker.*", "s2p_vif", s2p_if);
  run_test("basic_data_transform_test");
end



/*
//每次请求新数据信号正跳边沿，等一段时间后将输出数据增加1
always @(posedge ask_for_data)
begin
  #(`halfperiod/2 +3) data = data +1; 
end
*/




endmodule