package dut_pkg;
  
  import uvm_pkg::*;
  `include "uvm_macros.svh"
  import d2s_pkg::*;
  import s2p_pkg::*;
  
  class dut_refmod extends uvm_component;
    uvm_blocking_get_peek_port #(d2s_mon_trans) in_bgpk_port;
	uvm_tlm_analysis_fifo #(s2p_trans) out_tlm_fifo;
    
    `uvm_component_utils(dut_refmod) 
    function new (string name = "dut_refmod", uvm_component parent);
      super.new(name, parent);
      in_bgpk_port = new(("in_bgpk_port"), this);
      out_tlm_fifo = new(("out_tlm_fifo"), this);
    endfunction	

    task run_phase(uvm_phase phase);
	  do_ref();
    endtask	
	
	task do_ref();
	  d2s_mon_trans d2s;
	  s2p_trans s2p;
	  int i;
	  int dut_cnt = 0;
	  int length = 64;
	  logic [3:0] data_buf = 0;
	  forever begin	  
	    this.in_bgpk_port.get(d2s);
		`uvm_info(get_type_name(),$sformatf("refmod get d2s data : %h", d2s.data), UVM_MEDIUM)
		data_buf = d2s.data;
		s2p = new();
		if(data_buf == 0) begin
		  s2p.data_16b = 16'h8000;
		  this.out_tlm_fifo.put(s2p);	
		  continue;
		end		
		else begin//如果输入的数不为0，则找出其哪一位为1
		  s2p.data_16b = 16'd0;
		  s2p.data_16b[data_buf-1] = 1;//这就是模块实现的主要功能，但是使用了不一样的方法，这才是验证需要与设计分开的原因
		  	  
		end
		
		if(dut_cnt == 1) begin	//获取传输数据个数
		  length = d2s.data;
		end
		else if(dut_cnt == length + 1) begin 
		  dut_cnt = 0;
		end
		dut_cnt = dut_cnt +1;
		this.out_tlm_fifo.put(s2p);	
		`uvm_info(get_type_name(),$sformatf("refmod sent d2s expect data : %h", s2p.data_16b), UVM_MEDIUM)
	  end	
	endtask
  endclass: dut_refmod



  class dut_coverage extends uvm_component;
    local virtual s2p_intf s2p_vif;
    `uvm_component_utils(dut_coverage)
	
	covergroup outdata;
	  coverpoint s2p_vif.mon_ck.data_16b{
	    bins is_0 = {16'h8000};
		bins is_1 = {16'h0001};
		bins is_2 = {16'h0002};
		bins is_3 = {16'h0004};
		bins is_4 = {16'h0008};
		bins is_5 = {16'h0010};
		bins is_6 = {16'h0020};
		bins is_7 = {16'h0040};
		bins is_8 = {16'h0080};
		bins is_9 = {16'h0100};
		bins is_10 = {16'h0200};
		bins is_11 = {16'h0400};
		bins is_12 = {16'h0800};
		bins is_13 = {16'h1000};
		bins is_14 = {16'h2000};
		bins is_15 = {16'h4000};	
	  }	
	endgroup	
	
    function new(string name = "dut_coverage",uvm_component parent);
	  super.new(name,parent);	
	  this.outdata = new();
	endfunction
	
	function void connect_phase(uvm_phase phase);
	  if(!uvm_config_db#(virtual s2p_intf)::get(this,"","s2p_vif", s2p_vif)) 
        `uvm_fatal("GETVIF","cannot get s2p_vif handle from config DB")	
	endfunction
	
	task run_phase(uvm_phase phase);
	  this.do_output_sample();
	endtask
	
	task do_output_sample();
	  forever begin
	    @(posedge s2p_vif.sclk iff s2p_vif.rstn);
		outdata.sample();		
	  end
	endtask:do_output_sample	
	
	function void report_phase(uvm_phase phase);
      string s;
      super.report_phase(phase);
      s = "\n---------------------------------------------------------------\n";
      s = {s, "COVERAGE SUMMARY \n"}; 
      s = {s, $sformatf("total coverage: %.1f \n", $get_coverage())}; 
      s = {s, $sformatf("  cg_outdata coverage: %.1f \n", this.outdata.get_coverage())}; 
      s = {s, "---------------------------------------------------------------\n"};
      `uvm_info(get_type_name(), s, UVM_LOW)
	endfunction
	
  endclass: dut_coverage


  
  class dut_checker extends uvm_scoreboard;
	dut_refmod refmod;
	dut_coverage covrge;
    local int err_count;
    local int total_count;	
	
    `uvm_component_utils(dut_checker)
	
	uvm_tlm_analysis_fifo #(d2s_mon_trans) d2s_tlm_fifo;//连接 refmod(input),输入接口捕获的trans
	uvm_tlm_analysis_fifo #(s2p_trans) s2p_tlm_fifo;//连接 compare,
	uvm_blocking_get_port #(s2p_trans) exp_bg_port;//连接 refmod(output),期望值
	
    function new(string name = "dut_checker", uvm_component parent);
	  super.new(name, parent);
	  d2s_tlm_fifo = new("d2s_tlm_fifo",this);
	  s2p_tlm_fifo = new("s2p_tlm_fifo",this);
	  exp_bg_port = new("exp_bg_port",this);
	  this.err_count = 0;
      this.total_count = 0;
	endfunction
	
	function void build_phase(uvm_phase phase);
      super.build_phase(phase);
	  refmod = dut_refmod::type_id::create("refmod",this);
	  this.covrge = dut_coverage::type_id::create("covrge",this);
	endfunction
	
	function void connect_phase(uvm_phase phase);
      super.connect_phase(phase);
	  refmod.in_bgpk_port.connect(d2s_tlm_fifo.blocking_get_peek_export);
	  exp_bg_port.connect(refmod.out_tlm_fifo.blocking_get_export);
	endfunction
	
	task run_phase(uvm_phase phase);
	  do_data_compare();
	endtask
	
	extern task do_data_compare();	
	
	function void report_phase(uvm_phase phase);	
	endfunction
  endclass: dut_checker

  task dut_checker::do_data_compare();
    s2p_trans expt, mont;
	bit cmp;
	forever begin
	  this.s2p_tlm_fifo.get(mont);
	  this.exp_bg_port.get(expt);
	  cmp = mont.compare(expt);  
	  this.total_count++;
        if(cmp == 0) begin
          this.err_count++; #1ns;
          `uvm_info("[CMPERR]", $sformatf("monitored s2p data : %h", mont.data_16b), UVM_MEDIUM)
          `uvm_info("[CMPERR]", $sformatf("expected s2p data : %h", expt.data_16b), UVM_MEDIUM)
          `uvm_error("[CMPERR]", $sformatf("%0dth times comparing but failed! dut monitored output packet is different with reference model output", this.total_count))
        end
        else begin
		  `uvm_info("[CMPSUC]", $sformatf("monitored s2p data : %h", mont.data_16b), UVM_MEDIUM)
          `uvm_info("[CMPSUC]", $sformatf("expected s2p data : %h", expt.data_16b), UVM_MEDIUM)
          `uvm_info("[CMPSUC]",$sformatf("%0dth times comparing and succeeded! dut monitored output packet is the same with reference model output", this.total_count), UVM_LOW)
        end	  
	end 
  endtask: do_data_compare

  
  
  class top_virtual_sequencer extends uvm_sequencer #(uvm_sequence_item);
    d2s_sequencer d2s_sqr;
	`uvm_component_utils(top_virtual_sequencer)
   
    function new (string name = "top_virtual_sequencer", uvm_component parent);
      super.new(name, parent);
    endfunction
    
    function void build_phase(uvm_phase phase);
      super.build_phase(phase);		  
	endfunction
  endclass: top_virtual_sequencer



  class dut_env extends uvm_env;
    d2s_agent d2s_agt;
	s2p_agent s2p_agt;
	dut_checker chker;
	top_virtual_sequencer top_vir_sqr;
    `uvm_component_utils(dut_env)	
    function new(string name = "dut_env", uvm_component parent);
	  super.new(name,parent);	
	endfunction

    function void build_phase(uvm_phase phase);
      super.build_phase(phase);
	  this.d2s_agt = d2s_agent::type_id::create("d2s_agt",this);
	  this.s2p_agt = s2p_agent::type_id::create("s2p_agt",this);
	  this.chker = dut_checker::type_id::create("chker",this);
	  this.top_vir_sqr = top_virtual_sequencer::type_id::create("top_vir_sqr",this);
	endfunction

    function void connect_phase(uvm_phase phase);
      super.connect_phase(phase);	 
	  this.top_vir_sqr.d2s_sqr = this.d2s_agt.sequencer;
	  d2s_agt.monitor.mon_ana_port.connect(chker.d2s_tlm_fifo.analysis_export);
	  s2p_agt.monitor.mon_ana_port.connect(chker.s2p_tlm_fifo.analysis_export);
	endfunction
  endclass: dut_env



  class dut_top_sequence extends uvm_sequence #(uvm_sequence_item);
    d2s_data_sequence d2s_data_seq;
	
    `uvm_object_utils(dut_top_sequence)
    `uvm_declare_p_sequencer(top_virtual_sequencer)

    function new (string name = "dut_top_sequence");
      super.new(name);
    endfunction    
 
    virtual task body();
      `uvm_info(get_type_name(), "=====================STARTED=====================", UVM_LOW)      
	  fork
		this.data_transform();
	  join
      `uvm_info(get_type_name(), "=====================FINISHED=====================", UVM_LOW)
    endtask

    virtual task data_transform();
      //User to implement the task in the child virtual sequence
    endtask	
  endclass: dut_top_sequence



  class dut_top_test extends uvm_test;
    dut_env env;
    `uvm_component_utils(dut_top_test)

    function new(string name = "dut_top_test", uvm_component parent);
      super.new(name, parent);
    endfunction

    function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      env = dut_env::type_id::create("env", this);
    endfunction

    function void end_of_elaboration_phase(uvm_phase phase);
      super.end_of_elaboration_phase(phase);
      uvm_root::get().set_report_verbosity_level_hier(UVM_HIGH);
      uvm_root::get().set_report_max_quit_count(1);
      uvm_root::get().set_timeout(10ms);
    endfunction

    task run_phase(uvm_phase phase);
	  `uvm_info("[RUN_PHASE","we can display before raising objection",UVM_HIGH)

      phase.raise_objection(this);
      this.run_top_virtual_sequence();
	  #300us
      phase.drop_objection(this);
    endtask

    virtual task run_top_virtual_sequence();
      // User to implement this task in the child tests
    endtask  
  endclass: dut_top_test



  class basic_data_transform_vir_sequence extends dut_top_sequence;
    `uvm_object_utils(basic_data_transform_vir_sequence)
    function new (string name = "basic_data_transform_vir_sequence");
      super.new(name);
    endfunction    
    task data_transform();
	  `uvm_do_on_with(d2s_data_seq,p_sequencer.d2s_sqr,
						{data_size == 16;data_ud_dly == 2;} )	
	endtask
  endclass: basic_data_transform_vir_sequence



  class basic_data_transform_test extends dut_top_test;
    `uvm_component_utils(basic_data_transform_test)

    function new(string name = "basic_data_transform_test", uvm_component parent);
      super.new(name, parent);
    endfunction

    task run_top_virtual_sequence();
      basic_data_transform_vir_sequence top_seq = new();
      top_seq.start(env.top_vir_sqr);
    endtask     
  endclass: basic_data_transform_test


endpackage