module data_to_serial(
	input rstn,
	input sclk,
	output reg ack,
	output reg scl,
	output sda,
	input [3:0] data	
	);
	
	reg link_sda, sdabuf;
	reg [3:0] databuf;
	reg [7:0] data_in_to_sda_state;
	
	assign sda = link_sda? sdabuf:1'b0;
	
	parameter ready = 8'b0000_0000,
		      start = 8'b0000_0001,
			  data_in_bit3_start = 8'b0000_0010,
		      data_in_bit2_start = 8'b0000_0100,
			  data_in_bit1_start = 8'b0000_1000,
		      data_in_bit0_start = 8'b0001_0000,
			  data_in_end = 8'b0010_0000,
		      stop = 8'b0100_0000,
			  IDLE = 8'b1000_0000;
			  
	always @(posedge sclk or negedge rstn)//scl保持正跳变持续反转
	  begin 
	    if(!rstn)
		  scl <= 1;
		else
		  scl <= ~scl;
	  end
	  
	always @(posedge ack)//在ack响应后立刻把data写入databuf，这个data就是输入到这个模块的data
	  databuf <= data;
	  
	always @(negedge sclk or negedge rstn)//注意sdabuf是要驱动out16hi的sda，所以需要满足一些时序要求
	begin
	  if(!rstn)
	  begin
	    link_sda <= 0;
		data_in_to_sda_state <= ready;
		sdabuf <= 1;
		ack <= 0;
	  end
	else begin
		case(data_in_to_sda_state)
		ready: if(ack)
			     begin
				   link_sda <= 1;
				   data_in_to_sda_state <= start;
				 end
			   else 
			     begin
				   link_sda<=0;
				   data_in_to_sda_state<=ready;
				   ack<=1;
				 end
		start: if(scl && ack)
		         begin
				   sdabuf <= 0;//按照时序图看，确实应该先高位后低位，依照时序图看更简单一些
				   data_in_to_sda_state <= data_in_bit3_start;
				 end
			   else data_in_to_sda_state <= start;
	    data_in_bit3_start: if(!scl)//只有scl为低的时候，sda才可以变动，此时的sda数据是无效的，因此可以在这时候，buffer里的值是稳定的
				begin
				  sdabuf <= databuf[3];
				  data_in_to_sda_state <= data_in_bit2_start;
				  ack <= 0;
				end
			  else data_in_to_sda_state <= data_in_bit3_start;
		data_in_bit2_start: if(!scl)
			    begin
				  sdabuf<=databuf[2];
				  data_in_to_sda_state<=data_in_bit1_start;
				end
				else data_in_to_sda_state<=data_in_bit2_start;
		data_in_bit1_start: if(!scl)
				begin
				  sdabuf<=databuf[1];
				  data_in_to_sda_state<=data_in_bit0_start;
				end
			  else data_in_to_sda_state <= data_in_bit1_start;
		data_in_bit0_start: if(!scl)
				begin
				  sdabuf<=databuf[0];
				  data_in_to_sda_state<=data_in_end;
				end
				else data_in_to_sda_state <= data_in_bit0_start;
		data_in_end: if(!scl)
				begin
				  sdabuf<=0;
				  data_in_to_sda_state<=stop;
				end
				else data_in_to_sda_state<=data_in_end;
		stop: if(scl)
				begin
				  sdabuf<=1;
				  data_in_to_sda_state<=IDLE;
				end
				else data_in_to_sda_state<=stop;
		IDLE: begin
				link_sda<=0;
				sdabuf<=1;
				data_in_to_sda_state<=ready;
			  end
		endcase
	end
	end
	
	endmodule
			   
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
			  