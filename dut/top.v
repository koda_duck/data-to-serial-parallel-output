module top(
  	input rstn,
	input sclk,
	input [3:0] data,
	output ack,
	output [15:0] parallel_out
//	output scl,
//	output sda
	);
	
	wire ack_d2s_top;
	wire scl_d2s_s2p;
	wire sda_d2s_s2p;
	
data_to_serial d2s_inst(
	.rstn(rstn),
	.sclk(sclk),
	.ack(ack_d2s_top),
	.scl(scl_d2s_s2p),
	.sda(sda_d2s_s2p),
	.data(data)	
	);

   assign ack = ack_d2s_top;
   
serial_to_parallel s2p_inst(
	.scl(scl_d2s_s2p),
	.sda(sda_d2s_s2p),
	.parallel_out(parallel_out)
	);
	
//	assign scl = scl_d2s_s2p;
//	assign sda = sda_d2s_s2p;
	
endmodule