module serial_to_parallel (//输出高16位，并行数据，仅有一位为高
	input scl,sda,
	output reg [15:0] parallel_out
	);
	
	reg [5:0] state;
	reg [3:0] sda_to_data, sda_to_databuf;
	reg StartFlag, EndFlag;
	
	always @(negedge sda)//开始标志逻辑
	  begin
	    if(scl)//scl=1，sda下降沿，开始
		  begin
		    StartFlag <= 1;
		  end
	    else if(EndFlag)//结束标志位，开始回0
		  StartFlag <= 0;
	  end
	  
	always @(posedge sda)//结束标志逻辑，串行数据由data写入databuf
	  if(scl)//scl=1，sda上边沿，结束
	  begin
	    EndFlag <= 1;
		sda_to_databuf <= sda_to_data;
	  end
	  else
	    EndFlag <= 0;
		
	parameter data_bit3_start = 6'b00_0001,
			  data_bit2_start = 6'b00_0010,
			  data_bit1_start = 6'b00_0100,
			  data_bit0_start = 6'b00_1000,
			  stop = 6'b01_0000;
			  
	always @(sda_to_databuf)//根据sda_to_databuf采集到的数据，相应输出对应位的并行数据
	  begin
	    case(sda_to_databuf)
		  4'b0001: parallel_out = 16'b0000_0000_0000_0001;
		  4'b0010: parallel_out = 16'b0000_0000_0000_0010;
		  4'b0011: parallel_out = 16'b0000_0000_0000_0100;
		  4'b0100: parallel_out = 16'b0000_0000_0000_1000;
		  4'b0101: parallel_out = 16'b0000_0000_0001_0000;
		  4'b0110: parallel_out = 16'b0000_0000_0010_0000;
		  4'b0111: parallel_out = 16'b0000_0000_0100_0000;
		  4'b1000: parallel_out = 16'b0000_0000_1000_0000;
		  4'b1001: parallel_out = 16'b0000_0001_0000_0000;
		  4'b1010: parallel_out = 16'b0000_0010_0000_0000;
		  4'b1011: parallel_out = 16'b0000_0100_0000_0000;
		  4'b1100: parallel_out = 16'b0000_1000_0000_0000;
		  4'b1101: parallel_out = 16'b0001_0000_0000_0000;
		  4'b1110: parallel_out = 16'b0010_0000_0000_0000;
		  4'b1111: parallel_out = 16'b0100_0000_0000_0000;
		  4'b0000: parallel_out = 16'b1000_0000_0000_0000;
		endcase
	  end
	  
	always @(posedge scl)
	  if(StartFlag)
	    case(state)//有限状态机，有的把状态转换写到单独的always块中，把数据串行写入sda_to_data，
		  data_bit3_start: begin
				   state <= data_bit2_start;
				   sda_to_data[3] <= sda;
				   $display("Now is in data_bit3_start!!!");
				 end
				 
		  data_bit2_start: begin
				   state <= data_bit1_start;
				   sda_to_data[2] <= sda;
				   $display("Now is in data_bit2_start!!!");
				 end  
		  
		  data_bit1_start: begin
				   state <= data_bit0_start;
				   sda_to_data[1] <= sda;
				   $display("Now is in data_bit1_start!!!");
				 end
		  
		  data_bit0_start: begin
				   state <= stop;
				   sda_to_data[0] <= sda;
				   $display("Now is in data_bit0_start!!!");
				 end
				 
		  stop: begin
				   state <= data_bit3_start;
				   $display("Now is in stop.It will return data_bit3_start to receive next serial data!!!");
				 end
				 
		  default: state <= data_bit3_start;
		  endcase
		  
		else state <= data_bit3_start;
		
	endmodule 
	  
	  
	  
	  
	  
		